//------ Activity No. 3 - retrieve all the to do list items-----

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => {
	console.log(data);
})


// -----Activity No.4 - create an array using the map method to return just the title-----
// let map = data.map()
// fetch('https://jsonplaceholder.typicode.com/todos%27)
// .then((response) => response.json())
// .then((data) => {
//      data.map((elem) => arr.push(elem.title) {
//      console.log(elem)
//   })

// })

// -----Activity No.5 - create fetch request using the GET method that will retrieve a single to do list-----
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	}

})
.then((response) => response.json())
.then((data) => console.log(data))


// -----Activity No.6 - print a message in the console that will provide the title and status of the to do list item-----

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((data) => console.log(data))


// -----Activity No.7 - create fetch request using the POST method that will create a to do list item-----

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New Post',
		body: 'Hello Friend!, New Post',
		userId: 1
	})

})
.then((response) => response.json())
.then((data) => console.log(data))


// -----Activity No.8 - create fetch request using the PUT method that will update a to do list item-----

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated Item',
	})

})
.then((response) => response.json())
.then((data) => console.log(data))


// -----Activity No.9-  changing data structure-----

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		Title: 'Add title',
		Description: 'Add description',
		Status: 'Add status',
		DateCompleted: 'Add date',
		UserID: 'Add Id'
	})

})
.then((response) => response.json())
.then((data) => console.log(data))

// -----Activity No.10-  PATCH method to update a to do list item-----
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Another Updated Item',
	})

})
.then((response) => response.json())
.then((data) => console.log(data))

// -----Activity No.11-  update a status to complete and add a date when the status was changed-----
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: true,
		DateCompleted: 'October 3,2022'
	})

})
.then((response) => response.json())
.then((data) => console.log(data))

// -----Activity No.12-  delete an item -----
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'DELETE'

})



